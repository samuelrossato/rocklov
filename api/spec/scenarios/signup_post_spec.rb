describe " POST/ signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      #dado que eu tenho um novo suario
      payload = { name: "João da Silva", email: "joao@ig.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      #e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)
      #quando faço uma requisição para a rota /signup com o mesmo email
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      #então deve retornar 409, usuario ja cadastrado
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  #dessa forma deixamos toda a massa de dados dentro da variavel examples
  # examples = [
  #   {
  #     title: "usuario ja existe",
  #     payload: { name: "João da Silva", email: "joao@ig.com.br", password: "pwd123" },
  #     code: 409,
  #     error: "Email already exists :(",
  #   },
  #   {
  #     title: "nome é obrigatório",
  #     payload: { name: "", email: "semnome@ig.com.br", password: "pwd123" },
  #     code: 412,
  #     error: "required name",
  #   },
  #   {
  #     title: "email é obrigatório",
  #     payload: { name: "Samuel", email: "", password: "pwd123" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "password é obrigatório",
  #     payload: { name: "Samuel", email: "samuel@yahoo.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  # # e criamos uma laço de repetição " each " a partir da variavel examples passando um contador " e "
  # examples.each do |e|
  #   context "#{e[:title]}" do
  #     before(:all) do
  #       @result = Signup.new.create(e[:payload])
  #     end

  #     it "deve retornar #{e[:code]}" do
  #       expect(@result.code).to eql e[:code]
  #     end

  #     it "deve retornar mensagem: #{e[:error]}" do
  #       expect(@result.parsed_response["error"]).to eql e[:error]
  #     end
  #   end
  # end

  # context "nome é obrigatório" do
  #   before(:all) do
  #     payload = { name: "", email: "semnome@ig.com.br", password: "pwd123" }
  #     Signup.new.create(payload)
  #     @result = Signup.new.create(payload)
  #   end

  #   it "deve retornar 412" do
  #     #então deve retornar 409, usuario ja cadastrado
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required name"
  #   end
  # end

  # context "email é obrigatório" do
  #   before(:all) do
  #     payload = { name: "Samuel", email: "", password: "pwd123" }
  #     Signup.new.create(payload)
  #     @result = Signup.new.create(payload)
  #   end

  #   it "deve retornar 412" do
  #     #então deve retornar 409, usuario ja cadastrado
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required email"
  #   end
  # end

  # context "password é obrigatório" do
  #   before(:all) do
  #     payload = { name: "Samuel", email: "samuel@yahoo.com", password: "" }
  #     Signup.new.create(payload)
  #     @result = Signup.new.create(payload)
  #   end

  #   it "deve retornar 412" do
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required password"
  #   end
  # end
end
